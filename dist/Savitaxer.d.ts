import { Totals } from './Totals';
import { TaxConfig } from './TaxConfig';
export declare class Savitaxer {
    decimals: number;
    private taxConfigs;
    static addOperator(name: string, func: Function): any;
    static apply(logic?: any, data?: any, decimals?: number): number;
    static copy(val: any): any;
    exec(country: string, taxName: string, base: number, data?: any, inverse?: boolean): Totals;
    getTaxConfig(country: string, taxName: string): TaxConfig;
    getCountryTaxes(country: string): {
        [key: string]: TaxConfig;
    };
    addTax(country: string, taxName: string, taxConfig: TaxConfig, override?: boolean): boolean;
}
