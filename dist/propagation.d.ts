export declare type Calculus = (n: number) => number;
export declare function propagation(desired: number, func: Calculus, limit: number): number;
