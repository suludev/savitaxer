"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var categoriesTable = [
    {
        name: 'Gasolinas y diesel',
        subcategories: [
            { name: 'Menor a 92 octanos (Magna)', fee: 4.59, id: 1 },
            { name: 'Mayor o igual a 92 octanos (Premium)', fee: 3.88, id: 2 },
            { name: 'Diesel', fee: 5.04, id: 3 }
        ],
        id: 1
    },
    {
        name: 'Combustibles f\xF3siles',
        subcategories: [
            { name: 'Propano', fee: 0.0693, id: 1 },
            { name: 'Butano', fee: 0.0898, id: 2 },
            { name: 'Gasolinas y gasavi\xF3n', fee: 0.1217, id: 3 },
            { name: 'Turbosina y otros kerosenos', fee: 0.1454, id: 4 },
            { name: 'Di\xE9sel', fee: 0.1476, id: 5 },
            { name: 'Combust\xF3leo', fee: 0.1576, id: 6 },
            { name: 'Coque de petr\xF3leo', fee: 0.1829, id: 7 },
            { name: 'Coque de carb\xF3n', fee: 0.4288, id: 8 },
            { name: 'Carb\xF3n mineral', fee: 0.3229, id: 9 }
        ],
        id: 2
    },
    {
        name: 'Bebidas con contenido alcoh\xF3lico y cerveza',
        subcategories: [
            { name: 'Con una graduaci\xF3n alcoh\xF3lica de hasta 14\xB0 GL', rate: 26.5, id: 0 },
            {
                name: 'Con una graduaci\xF3n alcoh\xF3lica de m\xE1s de 14\xB0 y hasta 20\xB0GL',
                rate: 30,
                id: 1
            },
            { name: 'Con una graduaci\xF3n alcoh\xF3lica de m\xE1s de 20\xB0GL', rate: 53, id: 2 }
        ],
        id: 3
    },
    {
        name: 'Alcohol, alcohol desnaturalizado y mieles incristalizables',
        rate: 50,
        id: 4
    },
    {
        name: 'Tabacos labrados',
        subcategories: [
            { name: 'Cigarros', rate: 160, id: 1 },
            { name: 'Puros y otros tabacos labrados', rate: 160, id: 2 },
            { name: 'Puros y otros tabacos labrados hechos enteramente a mano', rate: 30.4, id: 3 }
        ],
        id: 5
    },
    {
        name: 'Bebidas energetizantes',
        details: 'As\xED como concentrados, polvos y jarabes para preparar bebidas energetizantes',
        rate: 25,
        id: 6
    },
    {
        name: 'Bebidas saborizadas',
        details: 'concentrados, polvos, jarabes, esencias o extractos de sabores, que al diluirse permitan obtener bebidas saborizadas; y jarabes o concentrados para preparar bebidas saborizadas que se expendan en envases abiertos utilizando aparatos autom\xE1ticos, el\xE9ctricos o mec\xE1nicos, siempre que los bienes a que se refiere este inciso contengan cualquier tipo de az\xFAcares a\xF1adidos',
        fee: 1,
        id: 7
    },
    {
        name: 'Plaguicidas',
        subcategories: [
            { name: 'Categor\xEDas 1 y 2', rate: 9, id: 1 },
            { name: 'Categor\xEDa 3', rate: 7, id: 2 }
        ],
        id: 8
    }
];
var iepsRule = {
    '+': [
        {
            '*': [
                {
                    '/': [
                        {
                            var: ['rate', 0,
                                {
                                    temp: ['subcategory',
                                        {
                                            find: [
                                                {
                                                    var: ['subcategories', [],
                                                        {
                                                            temp: ['category',
                                                                {
                                                                    find: [
                                                                        { var: 'categoriesTable' },
                                                                        { '===': [{ var: '$item.id' }, { var: 'category' }] }
                                                                    ]
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    '===': [
                                                        { var: '$item.id' },
                                                        { var: 'subcategory' }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }]
                        },
                        100
                    ]
                },
                { var: 'base' }
            ]
        },
        { '*': [{ var: ['$subcategory.fee', 0] }, { var: 'quantity' }] },
        { '*': [{ var: 'base' }, { var: ['$category.rate', 0] }] },
        { '*': [{ var: ['$category.fee', 0] }, { var: 'quantity' }] }
    ]
};
exports.ieps = {
    logic: iepsRule,
    inverse: {},
    data: {
        base: 0,
        quantity: 1,
        category: 0,
        subcategory: 0,
        categoriesTable: categoriesTable
    }
};
//# sourceMappingURL=ieps.js.map