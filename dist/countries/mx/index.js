"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var iva_1 = require("./iva");
var ieps_1 = require("./ieps");
var isr_1 = require("./isr");
exports.mx = {
    iva: iva_1.iva,
    isr: isr_1.isr,
    ieps: ieps_1.ieps
};
//# sourceMappingURL=index.js.map