"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.iva = {
    logic: {
        '*': [
            { 'var': 'base' },
            { 'var': 'rate' }
        ]
    },
    inverse: {
        '/': [
            { 'var': 'base' },
            { '+': [1, { 'var': 'rate' }] }
        ]
    },
    data: {
        base: 0,
        rate: 0.16
    }
};
//# sourceMappingURL=iva.js.map