"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function propagation(desired, func, limit) {
    limit = limit || 20;
    var error = 1;
    var estimated = desired / 2;
    while (!!error && !!limit--) {
        var result = estimated - func(estimated);
        error = (desired - result) / desired;
        estimated = estimated / (1 - error);
    }
    return estimated;
}
exports.propagation = propagation;
//# sourceMappingURL=propagation.js.map