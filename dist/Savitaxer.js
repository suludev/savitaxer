"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var JsonLogic = require("json-logic-js");
var mathjs = require("mathjs");
var countries_1 = require("./countries");
var propagation_1 = require("./propagation");
var Savitaxer = /** @class */ (function () {
    function Savitaxer() {
        this.decimals = 2;
        this.taxConfigs = countries_1.countries;
    }
    Savitaxer.addOperator = function (name, func) {
        return JsonLogic.add_operation(name, func);
    };
    Savitaxer.apply = function (logic, data, decimals) {
        if (logic === void 0) { logic = {}; }
        if (decimals === void 0) { decimals = 2; }
        JsonLogic.decimals = decimals;
        return mathjs.round(JsonLogic.apply(logic, data), decimals);
    };
    Savitaxer.copy = function (val) {
        return JSON.parse(JSON.stringify(val));
    };
    Savitaxer.prototype.exec = function (country, taxName, base, data, inverse) {
        if (data === void 0) { data = {}; }
        if (inverse === void 0) { inverse = false; }
        country = country.toLowerCase();
        taxName = taxName.toLowerCase();
        var taxConfig = this.getTaxConfig(country, taxName);
        data = __assign({}, taxConfig.data, data, { base: base });
        if (inverse) {
            JsonLogic.add_operation('propagation', propagation_1.propagation);
            data.calcFunction = function (base) {
                var newData = __assign({}, data);
                newData.base = base;
                return Savitaxer.apply(taxConfig.logic, newData);
            };
        }
        data.taxAmount = Savitaxer.apply(inverse ? taxConfig.inverse : taxConfig.logic, data, this.decimals);
        return data;
    };
    Savitaxer.prototype.getTaxConfig = function (country, taxName) {
        country = country.toLowerCase();
        taxName = taxName.toLowerCase();
        if (!this.taxConfigs[country]) {
            return this.taxConfigs[country];
        }
        if (!this.taxConfigs[country][taxName]) {
            return this.taxConfigs[country][taxName];
        }
        return Savitaxer.copy(this.taxConfigs[country][taxName]);
    };
    Savitaxer.prototype.getCountryTaxes = function (country) {
        country = country.toLowerCase();
        if (!this.taxConfigs[country]) {
            return this.taxConfigs[country];
        }
        return Savitaxer.copy(this.taxConfigs[country]);
    };
    Savitaxer.prototype.addTax = function (country, taxName, taxConfig, override) {
        if (override === void 0) { override = false; }
        var countryConfigs = this.taxConfigs[country] || {};
        if (!countryConfigs[taxName] || override) {
            countryConfigs[taxName] = taxConfig;
            return true;
        }
        else {
            return false;
        }
    };
    return Savitaxer;
}());
exports.Savitaxer = Savitaxer;
//# sourceMappingURL=Savitaxer.js.map