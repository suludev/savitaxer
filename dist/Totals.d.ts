export interface Totals {
    base: number;
    taxAmount: number;
    totalAmount: number;
    fee: number;
    rate: number;
    multiplier: number;
    data?: any;
}
