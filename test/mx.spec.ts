
import { expect } from 'chai';
import 'mocha';
import { Savitaxer } from '../src/Savitaxer';

const savitaxer = new Savitaxer();

describe('Mexico', () => {

  describe('IVA', () => {

    it('The iva of 200 most be 32', () => {
      expect(savitaxer.exec('mx', 'iva', 200).taxAmount).equal(32);
    });

    it('The iva of 1278.56 most be 204.57', () => {
      expect(savitaxer.exec('mx', 'iva', 1278.56).taxAmount).equal(204.57);
    });

    it('The iva of 1187.23 most be 189.96', () => {
      expect(savitaxer.exec('mx', 'iva', 1187.23).taxAmount).equal(189.96);
    });

    it('The iva of 1876.2334 most be 300.20', () => {
      expect(savitaxer.exec('mx', 'iva', 1876.2334).taxAmount).equal(300.20);
    });

    it('The inverse iva of 8000 must be 6,896.55', () => {
      expect(savitaxer.exec('mx', 'iva', 8000, {}, true).taxAmount).equals(6896.55);
    });

  });

  describe('ISR', () => {

    describe('Moral', () => {

      it('The isr of 2000 for moral most be 600', () => {
        expect(savitaxer.exec('mx', 'isr', 2000).taxAmount).equal(600);
      });

      it('The isr of 150455.43 for moral most be 45136.63', () => {
        expect(savitaxer.exec('mx', 'isr', 150455.43).taxAmount).equal(45136.63);
      });

      it('The inverse isr of 8000 for moral must be 6,153.85', () => {
        expect(savitaxer.exec('mx', 'isr', 8000, {isMoral: true}, true).taxAmount).equals(6153.85);
      });

    });

    describe('Not moral', () => {

      it('The isr of 2000 for not moral most be 102.08', () => {
        expect(savitaxer.exec('mx', 'isr', 2000, {isMoral: false}).taxAmount).equal(102.08);
      });

      it('The isr of 15544 for not moral most be 2026.74', () => {
        expect(savitaxer.exec('mx', 'isr', 15544, {isMoral: false}).taxAmount).equal(2026.74);
      });

      it('The inverse isr of 8000 for not moral must be 8,705.10', () => {
        expect(savitaxer.exec('mx', 'isr', 8000, {isMoral: false}, true).taxAmount).equals(8705.10);
      });

    });

  });

});
