export type Calculus = (n: number) => number;

export function propagation(desired: number, func: Calculus, limit: number): number {
  limit = limit || 20;
  let error: number = 1;
  let estimated: number = desired / 2;
  while (!!error && !!limit--) {
    const result = estimated - func(estimated);
    error = (desired - result) / desired;
    estimated = estimated / (1 - error);
  }
  return estimated;
}