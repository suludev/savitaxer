
import * as JsonLogic from 'json-logic-js';
import * as mathjs from 'mathjs';
import { Totals } from './Totals';
import { countries } from './countries';
import { TaxConfig } from './TaxConfig';
import { propagation } from './propagation';

export class Savitaxer {

  public decimals = 2;
  private taxConfigs: any = countries;

  public static addOperator(name: string, func: Function): any {
    return JsonLogic.add_operation(name, func);
  }

  public static apply(logic: any = {}, data?: any, decimals: number = 2): number {
    (JsonLogic as any).decimals = decimals;
    return <number>mathjs.round(JsonLogic.apply(logic, data), decimals);
  }

  public static copy(val: any): any {
    return JSON.parse(JSON.stringify(val));
  }

  public exec(country: string, taxName: string, base: number, data: any = {}, inverse: boolean = false): Totals {
    country = country.toLowerCase();
    taxName = taxName.toLowerCase();
    const taxConfig = this.getTaxConfig(country, taxName);
    data = <any>{ ...taxConfig.data, ...data, base };
    if (inverse) {
      JsonLogic.add_operation('propagation', propagation);
      data.calcFunction = (base: number) => {
        const newData = {...data};
        newData.base = base;
        return Savitaxer.apply(taxConfig.logic, newData);
      };
    }
    data.taxAmount = Savitaxer.apply(inverse ? taxConfig.inverse : taxConfig.logic, data, this.decimals);
    return data;
  }

  public getTaxConfig(country: string, taxName: string): TaxConfig {
    country = country.toLowerCase();
    taxName = taxName.toLowerCase();
    if (!this.taxConfigs[country]) {
      return this.taxConfigs[country];
    }
    if (!this.taxConfigs[country][taxName]) {
      return this.taxConfigs[country][taxName];
    }
    return Savitaxer.copy(this.taxConfigs[country][taxName]);
  }

  public getCountryTaxes(country: string): { [key: string]: TaxConfig } {
    country = country.toLowerCase();
    if (!this.taxConfigs[country]) {
      return this.taxConfigs[country];
    }
    return Savitaxer.copy(this.taxConfigs[country]);
  }

  public addTax(country: string, taxName: string, taxConfig: TaxConfig, override: boolean = false): boolean {
    const countryConfigs = this.taxConfigs[country] || {};

    if (!countryConfigs[taxName] || override) {
      countryConfigs[taxName] = taxConfig;
      return true;
    } else {
      return false;
    }
  }

}
