import { iva } from './iva';
import { ieps } from './ieps';
import { isr } from './isr';
export const mx: any = {
  iva,
  isr,
  ieps
};