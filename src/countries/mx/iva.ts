
import { TaxConfig } from '../../TaxConfig';

export const iva: TaxConfig = {
  logic: {
    '*': [
      { 'var': 'base' },
      { 'var': 'rate' }
    ]
  },
  inverse: {
    '/': [
      { 'var': 'base' },
      {'+': [1, { 'var': 'rate' }]}
    ]
  },
  data: {
    base: 0,
    rate: 0.16
  }
};
