import { TaxConfig } from '../../TaxConfig';
import * as JsonLogic from 'json-logic-js';

JsonLogic.add_operation('mx:isr:monthlyCalc', monthlyCalc);

const alternative = {'-': [
  {'+': [
    {'/': [
      {'*': [
        {'-': [
          {'var': 'base'},
          {'var': [
            'lowerLimit',
            0,
            {'temp': [
              'row',
              {'find': [
                {'var': 'monthlyTable'},
                {'or': [
                  {'!': {'var': '$item.upperLimit'}},
                  {'<=': [
                    {'var': '$item.lowerLimit'},
                    {'var': 'base'},
                    {'var': '$item.upperLimit'}
                  ]}
                ]}
              ]}
            ]}
          ]}
        ]},
        {'var': '$row.rate'}
      ]},
      100
    ]},
    {'var': '$row.fee'}
  ]},
  {'if': [
    {'var': 'hasSubsidy'},
    {'var': [
      'fee',
      0,
      {'find': [
          {'var': 'subsidyTable'},
          {'or': [
              {'!': {'var': '$item.upperLimit'}},
              {'<=': [
                {'var': '$item.lowerLimit'},
                {'var': 'base'},
                {'var': '$item.upperLimit'}
              ]}
          ]}
      ]}
    ]},
    0
  ]}
]};

const inverseAlternative = {'if': [
  {'var': 'isMoral'},
  {'/': [
    {'var': 'base'},
    {'+': [1, {'var': 'moralRate'}]}
  ]},
  {'propagation': [
    {'var': 'base'},
    {'var': 'calcFunction'},
    {'var': 'limit'}
  ]}
]};

const monthlyTable = [
  {
    lowerLimit: 0.01,
    upperLimit: 578.52,
    fee: 0,
    rate: 1.92
  },
  {
    lowerLimit: 578.53,
    upperLimit: 4910.18,
    fee: 11.11,
    rate: 6.40
  },
  {
    lowerLimit: 4910.19,
    upperLimit: 8629.20,
    fee: 288.33,
    rate: 10.88
  },
  {
    lowerLimit: 8629.21,
    upperLimit: 10031.07,
    fee: 692.96,
    rate: 16
  },
  {
    lowerLimit: 10031.08,
    upperLimit: 12009.94,
    fee: 917.26,
    rate: 17.92
  },
  {
    lowerLimit: 12009.95,
    upperLimit: 24222.31,
    fee: 1271.87,
    rate: 21.36
  },
  {
    lowerLimit: 24222.32,
    upperLimit: 38177.69,
    fee: 3880.44,
    rate: 23.52
  },
  {
    lowerLimit: 38177.70,
    upperLimit: 72887.50,
    fee: 7162.74,
    rate: 30
  },
  {
    lowerLimit: 72887.51,
    upperLimit: 97183.33,
    fee: 17575.69,
    rate: 32
  },
  {
    lowerLimit: 97183.34,
    upperLimit: 291550,
    fee: 25350.35,
    rate: 34
  },
  {
    lowerLimit: 291550.01,
    upperLimit: Infinity,
    fee: 91435.02,
    rate: 35
  }
];
const subsidyTable = [
  {
    lowerLimit: 0.01,
    upperLimit: 1768.96,
    fee: 407.02
  },
  {
    lowerLimit: 1768.97,
    upperLimit: 2653.38,
    fee: 406.83
  },
  {
    lowerLimit: 2653.39,
    upperLimit: 3472.84,
    fee: 406.62
  },
  {
    lowerLimit: 3472.85,
    upperLimit: 3537.87,
    fee: 392.77
  },
  {
    lowerLimit: 3537.88,
    upperLimit: 4446.15,
    fee: 382.46
  },
  {
    lowerLimit: 4446.16,
    upperLimit: 4717.18,
    fee: 354.23
  },
  {
    lowerLimit: 4717.19,
    upperLimit: 5335.42,
    fee: 324.87
  },
  {
    lowerLimit: 5335.43,
    upperLimit: 6224.67,
    fee: 294.63
  },
  {
    lowerLimit: 6224.68,
    upperLimit: 7113.90,
    fee: 253.54
  },
  {
    lowerLimit: 7113.91,
    upperLimit: 7382.33,
    fee: 217.61
  },
  {
    lowerLimit: 7382.34,
    upperLimit: Infinity,
    fee: 0
  }
];

export const isr: TaxConfig = {
  logic: {'if': [
    {'var': 'isMoral'},
    {'*': [
      {'var': 'base'},
      {'var': 'moralRate'}
    ]},
    alternative
  ]},
  inverse: inverseAlternative,
  data: {
    base: 0,
    isMoral: true,
    hasSubsidy: false,
    moralRate: 0.3,
    monthlyTable,
    subsidyTable
  }
};

function monthlyCalc(base: number): number {
  const referenceData = getDataFromTable(base, monthlyTable);
  return referenceData.fee
    + (referenceData.rate ? (referenceData.rate / 100) * (base - referenceData.lowerLimit) : 0);
}

function getDataFromTable(valToCompare: number, table: any) {
  let index = 0;
  let referenceData;
  for (; index < table.length; index++) {
    referenceData = table[index];
    if (valToCompare >= referenceData.lowerLimit && valToCompare <= referenceData.upperLimit) {
      return referenceData;
    }
  }
  return {
    lowerLimit: Infinity,
    upperLimit: Infinity,
    fee: 0,
    rate: 0
  };
}
